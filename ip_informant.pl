#!/usr/bin/perl
#18.01.2009

use Socket;
use LWP::UserAgent;
use Data::Dumper;
use Getopt::Long;

my $user_list;
my $user_output; 

my $opt_dns;
my $opt_http;
my $opt_ipinfo;
my $opt_hostinfo;
my $api_key		 = "a07edabf7fc77e2fd9f70824dd26e241bb241b85e6fa53ecdb671f2198560de4"; ##not so secret
my $act			 = 0;
my $http_timeout = 15;
my $output_data;
my @entry_array;


#0 - ip 1-short cc 2-full cc
sub getIpInfo
{
	$ip = $_[0];

	$req_url = sprintf("http://api.ipinfodb.com/v3/ip-country/?key=%s&ip=%s", $api_key, $ip);
	$lwp	 = LWP::UserAgent->new();
	$req	 = HTTP::Request->new(GET, $req_url);

	$lwp->timeout($http_timeout);

	if ($lwp && $req) {
		$data = $lwp->request($req);
		if ($data->is_success) {
			@expl_array = split(";", $data->content);

			if ($expl_array[0] eq "OK") {
				@ret_array  = ($expl_array[2], $expl_array[3], $expl_array[4]);
				return @ret_array;
			}	
		}
	}

	return NULL;
}

sub resolveDNS
{
	my ($domain_name,  $ref_array) = @_;
	my $bResult;

	$entry_ip = gethostbyname($domain_name);
	if (defined $entry_ip) {
		$bResult = 1;
		$dns_res = inet_ntoa($entry_ip);
	}
	else
	{
		$dns_res = "-";
	}

	printf("[DNS]\t %s\n", $dns_res);	
	push(@{$ref_array}, $dns_res);

	return $bResult;
}

sub reverseDNS
{
	my ($domain_name,  $ref_array) = @_;
	my  $bResult;

	$entry_data = gethostbyaddr(inet_aton($domain_name), AF_INET);

	if ($entry_data && defined($entry_data)) {
		$res_iphost	= $entry_data;
		$bResult	= 1;
	}
	else
	{
		$res_iphost = "-";
	}

	printf("[HOST]\t %s\n", $res_iphost);
	push(@{$ref_array}, $res_iphost);

	return $bResult;
}

sub getCountry
{
	my ($domain_name,  $ref_array) = @_;
	my  $bResult;

	@entry_data = getIpInfo($domain_name);

	if (@entry_data && exists $entry_data[2] && length($entry_data[2]) > 3) {
		$res_ipinfo	= $entry_data[2];
		$bResult	= 1;
	}
	else
	{
		$res_ipinfo = "-";
	}

	printf("[INFO]\t %s\n", $res_ipinfo);
	push(@{$ref_array}, $res_ipinfo);

	return $bResult;
}

sub webTest
{
	my ($domain_name,  $ref_array) = @_;
	my  $bResult;

	$lwp  = LWP::UserAgent->new();
	$req  = HTTP::Request->new(GET, "http://".$domain_name);

	$lwp->timeout($http_timeout);

	if ($lwp ne NULL && $req ne NULL) {
		$data = $lwp->request($req);

	if ($data->is_success && $data->code ne 400) {
		$res_http = $data->code;
		$bResult  = 1;
	}
	else
	{
		$res_http = "-";
	}

		printf("[HTTP]\t %s\n", $res_http);
		push(@get_array, $res_http);
	}

	return $bResult;
}




sub usage
{
	print
	"\n",
	"Usage:\n\n",
	"--in  [file]\tFile name with input domains or IP addresses to process\n",
	"--out [file]\tFile name to which script writes results\n",
	"--dns\t\tResolves given entry to IPV4 address\n",
	"--http\t\tAttempts to establish HTTP connection to given entry (port 80)\n",
	"--info\t\t\Retrieves the country location    for given entry\n",
	"--revdns\tRetrieves the reverse dns address for given entry\n\n",
	"Example 1: Resolve DNS and location of list inside cornet.txt:\n\n",
	"perl script.pl --in cornet.txt --dns --info\n\n",
	"Example 2: Resolve DNS and revDNS and check for HTTP server. Same list:\n\n",
	"perl script.pl --in cornet.txt --dns --revdns --http\n\n";
}

sub stripEntry
{
	$entry  = $_[0];
	$opt_sb = $_[1];
	$opt_sp = $_[2];
	$tmp	= $entry;

	#phase 1 - strip http
	if ($opt_sb eq true) {
		$tmp =~ s/http:\/\///g;
		$tmp =~ s/https:\/\///g;
		$tmp =~ s/ftp:\/\///g;
		$tmp =~ s/www.//g;
	}

	if ($opt_sp eq true) {
		$last_slash_pos = index($tmp, "/");

		if ($last_slash_pos != -1) {
			$tmp = substr($tmp, 0, $last_slash_pos);
		}	
	}
	
	return $tmp;
}


$res = GetOptions("in=s"        => \$user_list,
				  "out=s"		=> \$user_output,
		          "dns"			=> \$opt_dns,
		          "http"		=> \$opt_http,
		          "info"		=> \$opt_ipinfo,
		          "revdns"		=> \$opt_hostinfo);



if (!defined $user_list) {
	usage();
	exit;
}
else
{
	if (!$opt_dns && !$opt_http && !$opt_ipinfo && !$opt_hostinfo) {
		usage();
		exit;
	}
}

if ($opt_http || $opt_ipinfo || $opt_hostinfo) {
	if (!$opt_dns) {
		$opt_dns = true;
	}
}

#Stupid. Just verify whether given parameter is url.
if (not -e $user_list) {
	push(@list_content, $user_list);
}
else
{
	open inHandle, "<", $user_list;
	@list_content = <inHandle>;
}


foreach (@list_content) {
	chomp($_);

	$entry	   = $_;
	$bResolved = 0;

	printf("[ENTRY]\t %s\n", $entry);

	#resolve IP
	if ($opt_dns) {
		my $entry_stripped = stripEntry($entry, true, true);

		if ($entry_stripped) {
			$bResolved = resolveDNS($entry_stripped, \@dns_array);
		}
	}

	#reverse DNS
	if ($opt_hostinfo) {
		$entry_stripped = stripEntry($entry, true, true);

		if ($entry_stripped && $bResolved ne 0) {
			reverseDNS($entry_stripped, \@iphost_array);
		}
	}

	#ip/domain/info
	if ($opt_ipinfo) {
		$entry_stripped = stripEntry($entry, true, true);

		if ($entry_stripped && $bResolved ne 0) {
			getCountry($entry_stripped, @ipinfo_array);
		}
	}

	#HTTP GET and obtain status code
	if ($opt_http) {
		my $entry_stripped = stripEntry($entry, true, false);

		if ($entry_stripped && $bResolved ne 0) {
			webTest($entry_stripped, @get_array);
		}
	}

	push(@entry_array, $entry);
	print "\n";
}

if ($user_output) {

	for ($i = 0; $i < scalar(@entry_array); $i++) {
	$output_data .= "=============================================\r\n";

	$output_data .= sprintf("Input:\t%s\r\n", $entry_array[$i]);

	if (exists $dns_array[$i]) {
		$output_data .= sprintf("Dns:\t%s\r\n", $dns_array[$i]);
	}

	if (exists $iphost_array[$i]) {
		$output_data .= sprintf("RevDns:\t%s\r\n", $iphost_array[$i]);
	}

	if (exists $get_array[$i]) {
		$output_data .= sprintf("HTTP:\t%s\r\n", $get_array[$i]);
	}
	if (exists $ipinfo_array[$i]) {
		$output_data .= sprintf("GEO Country:\t%s\r\n", $ipinfo_array[$i]);
	}
	$output_data .= "=============================================\r\n";
}

if ($output_data) {
	open  outHandle, ">>", $user_output;
	print outHandle $output_data;
	close outHandle;
	printf("[+] Sucessfully wrote %d bytes to %s\n", length($output_data), $user_output);
}
}



close inHandle;